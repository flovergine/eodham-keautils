#!/usr/bin/env python
#
# Copyright (c) 2013 Francesco P Lovergine <frankie(at)gisgeek.org>
#
# Licensed under the EUPL 1.1
# See 
#   https://joinup.ec.europa.eu/software/page/eupl/licence-eupl
# for the text of license.
#
# This software is distributed WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the above copyright notices for more information.
#

try:
    from osgeo import gdal
except ImportError:
    import gdal

from gdalconst import *

try:
    progress = gdal.TermProgress_nocb
except:
    progress = gdal.TermProgress

try:
    import numpy as Numeric
except ImportError:
    import Numeric

import sys
import os.path
import re
import math
import random

try:
    import xlsxwriter as xls
except ImportError:
    print 'Missing XlsxWriter module'
    sys.exit(1)

from xlsxwriter.utility import xl_rowcol_to_cell

try:
    from rios import rat
except ImportError:
    print 'Missing Rios module'
    sys.exit(1)

__version__ = 1.0

verbose = False
simplify = False

def usage():
    print('''Usage: kea_changes.py [-o output_file] [-of output_format]
              [-ps pixelsize_x pixelsize_y] [-v] [-V] [-noct]
              [-ul_lr ulx uly lrx lry] [-f field] [-f1 field] [-f2 field]
              [-ot datatype] from_image to_image
''')    
    
class file_info:

    def init_from_name(self, filename):
        fh = gdal.Open( filename,GA_ReadOnly )
        if fh is None:
            return 0

        self.filename = filename
        self.bands = fh.RasterCount
        self.xsize = fh.RasterXSize
        self.ysize = fh.RasterYSize
        self.band_type = fh.GetRasterBand(1).DataType
        self.projection = fh.GetProjection()
        self.geotransform = fh.GetGeoTransform()
        self.ulx = self.geotransform[0]
        self.uly = self.geotransform[3]
        self.lrx = self.ulx + self.geotransform[1] * self.xsize
        self.lry = self.uly + self.geotransform[5] * self.ysize

        ct = fh.GetRasterBand(1).GetRasterColorTable()
        if ct is not None:
            self.ct = ct.Clone()
        else:
            self.ct = None

        return 1

    def get_filename(self):
        return self.filename

    def get_rat( self,field_name = 'P1_LCCS' ):
        table = rat.readColumn( self.filename,field_name )
        if verbose:
            print('Read RAT of %d element(s)' % len(table))
        return table

    def report( self ):
        if verbose:
            print('Filename: '+ self.filename)
            print('File Size: %dx%dx%d' \
                  % (self.xsize, self.ysize, self.bands))
            print('Pixel Size: %f x %f' \
                  % (self.geotransform[1],self.geotransform[5]))
            print('UL:(%f,%f)   LR:(%f,%f)' \
                  % (self.ulx,self.uly,self.lrx,self.lry))
            print('Proj: %s' + self.projection)

class file_transitions:

    def __init__ ( self,first,second ):
        self.first = first
        self.second = second
        self.first_fh = gdal.Open(first.filename, GA_ReadOnly)
        self.second_fh = gdal.Open(second.filename, GA_ReadOnly)
        self.bands = first.bands
        self.xsize = first.xsize
        self.ysize = first.ysize
        self.band_type = first.band_type
        self.projection = first.projection
        self.geotransform = first.geotransform
        self.ulx = self.geotransform[0]
        self.uly = self.geotransform[3]
        self.lrx = self.ulx + self.geotransform[1] * self.xsize
        self.lry = self.uly + self.geotransform[5] * self.ysize

    def raster_transitions( self,first_rat, second_rat, \
                         sw_xoff, sw_yoff, sw_xsize, sw_ysize, s_band_n, \
                         t_fh, tw_xoff, tw_yoff, tw_xsize, tw_ysize, t_band_n,
                         matrix, classes ):
        first_band = self.first_fh.GetRasterBand( s_band_n )
        second_band = self.second_fh.GetRasterBand( s_band_n )
        t_band = t_fh.GetRasterBand( t_band_n )

        first_data_src = first_band.ReadAsArray( sw_xoff, sw_yoff, sw_xsize, sw_ysize,
                                       tw_xsize, tw_ysize )
        second_data_src = second_band.ReadAsArray( sw_xoff, sw_yoff, sw_xsize, sw_ysize,
                                       tw_xsize, tw_ysize )
        data_dst = t_band.ReadAsArray( tw_xoff, tw_yoff, tw_xsize, tw_ysize )

        if verbose:
            print( 'Size is %d' % first_data_src.size)
            print( 'xoff is %d, yoff is %d' % (tw_xoff,tw_yoff))

        for i in range(first_data_src.size):
            data_dst.itemset(i, matrix[ \
                                (classes.index(first_rat[first_data_src.item(i)]), \
                                 classes.index(second_rat[second_data_src.item(i)]))])
        t_band.WriteArray( data_dst, tw_xoff, tw_yoff )

        return 0

    def transitions(self, t_fh, first_rat, second_rat, matrix, classes,  s_band =1, t_band = 1 ):
        t_geotransform = t_fh.GetGeoTransform()
        t_ulx = t_geotransform[0]
        t_uly = t_geotransform[3]
        t_lrx = t_geotransform[0] + t_fh.RasterXSize * t_geotransform[1]
        t_lry = t_geotransform[3] + t_fh.RasterYSize * t_geotransform[5]

        # figure out intersection region
        tgw_ulx = max(t_ulx,self.ulx)
        tgw_lrx = min(t_lrx,self.lrx)
        if t_geotransform[5] < 0:
            tgw_uly = min(t_uly,self.uly)
            tgw_lry = max(t_lry,self.lry)
        else:
            tgw_uly = max(t_uly,self.uly)
            tgw_lry = min(t_lry,self.lry)
        
        # do they even intersect?
        if tgw_ulx >= tgw_lrx:
            return 1
        if t_geotransform[5] < 0 and tgw_uly <= tgw_lry:
            return 1
        if t_geotransform[5] > 0 and tgw_uly >= tgw_lry:
            return 1
            
        # compute target window in pixel coordinates.
        tw_xoff = int((tgw_ulx - t_geotransform[0]) / t_geotransform[1] + 0.1)
        tw_yoff = int((tgw_uly - t_geotransform[3]) / t_geotransform[5] + 0.1)
        tw_xsize = int((tgw_lrx - t_geotransform[0])/t_geotransform[1] + 0.5) \
                   - tw_xoff
        tw_ysize = int((tgw_lry - t_geotransform[3])/t_geotransform[5] + 0.5) \
                   - tw_yoff

        if tw_xsize < 1 or tw_ysize < 1:
            return 1

        # Compute source window in pixel coordinates.
        sw_xoff = int((tgw_ulx - self.geotransform[0]) / self.geotransform[1])
        sw_yoff = int((tgw_uly - self.geotransform[3]) / self.geotransform[5])
        sw_xsize = int((tgw_lrx - self.geotransform[0]) \
                       / self.geotransform[1] + 0.5) - sw_xoff
        sw_ysize = int((tgw_lry - self.geotransform[3]) \
                       / self.geotransform[5] + 0.5) - sw_yoff

        if sw_xsize < 1 or sw_ysize < 1:
            return 1

        return \
            self.raster_transitions( first_rat, second_rat, \
                         sw_xoff, sw_yoff, sw_xsize, sw_ysize, s_band, \
                         t_fh, tw_xoff, tw_yoff, tw_xsize, tw_ysize, t_band, \
                         matrix, classes )

def names_to_fileinfos( names ):

    file_infos = []
    for name in names:
        fi = file_info()
        if fi.init_from_name( name ) == 1:
            file_infos.append( fi )

    return file_infos

def normalize( codes,simplify=True ):
    regex1 = re.compile('[-_]')
    regex2 = re.compile('\.+')
    regex3 = re.compile('\.$')
    regex4 = re.compile('[ABC][0-9]+')
    for i in range(0,len(codes)):
        norm = regex3.sub('',regex2.sub('.',regex1.sub('.',codes[i])))
        if simplify:
            code_list = norm.split('.')
            simple_code_list = []
            simple_code_list.append(code_list[0])
            '''
            if len(code_list) >= 2:
                simple_code_list.append(code_list[1])
                if code_list[0] == 'A12' or code_list[0] == 'A24':
                    for index in range(2,len(code_list)):
                        simple_code_list.append(regex4.sub('',code_list[index]))
                else:
                    for index in range(2,len(code_list)):
                        simple_code_list.append(code_list[index])
            '''
            norm = regex3.sub('',regex2.sub('.','.'.join(simple_code_list)))
        codes[i] = norm

def contrast_color(color):
    if color > 0x40: 
        return 0x00
    return 0xFF

def main( argv=None ):

    global verbose, __version__, simplify
    format = 'Gtiff'
    out_file = 'Changes.tif'
    bTargetAlignedPixels = False
    no_pct = False
    names = []
    ulx = None
    psize_x = None
    band_type = 'UInt16'
    field = 'P1_LCCS'
    field1 = None
    field2 = None

    gdal.AllRegister()
    if argv is None:
        argv = sys.argv
    argv = gdal.GeneralCmdLineProcessor( argv )
    if argv is None:
        sys.exit( 0 )

    i = 1
    while i < len(argv):
        arg = argv[i]

        if arg == '-o':
            i = i + 1
            out_file = argv[i]

        elif arg == '-of':
            i = i + 1
            format = argv[i]

        elif arg == '-v':
            verbose = True

        elif arg == '-noct':
            no_pct = True

        elif arg == '-ot':
            i = i + 1
            band_type = gdal.GetDataTypeByName( argv[i] )
            if band_type == gdal.GDT_Unknown:
                print('Unknown GDAL data type: %s' % argv[i])

        elif arg == '-ps':
            psize_x = float(argv[i+1])
            psize_y = -1 * abs(float(argv[i+2]))
            i = i + 2

        elif arg == '-ul_lr':
            ulx = float(argv[i+1])
            uly = float(argv[i+2])
            lrx = float(argv[i+3])
            lry = float(argv[i+4])
            i = i + 4

        elif arg == '-f':
            i = i + 1
            field = argv[i]

        elif arg == '-f1':
            i = i + 1
            field1 = argv[i]

        elif arg == '-f2':
            i = i + 1
            field2 = argv[i]

        elif arg == '-V':
            print '%s' % __version__
            sys.exit(1)

        elif arg == '-s':
            simplify = True

        elif arg[:1] == '-':
            print('Unrecognized command option: %s' % arg)
            usage()
            sys.exit(1)

        else:
            names.append(arg)

        i = i + 1

    if len(names) == 0:
        print('No KEA input files selected')
        usage()
        sys.exit(1)
    if len(names) != 2:
        print('Should pass two input KEA files')
        usage()
        sys.exit(1)

    Driver = gdal.GetDriverByName(format)
    if Driver is None:
        print('Format driver %s not found, pick a supported driver.' % format)
        sys.exit( 1 )
    metadata = Driver.GetMetadata()

    file_infos = names_to_fileinfos( names )

    first = file_infos[0]
    second = file_infos[1]

    for fi in file_infos:
        fi.report()

    if field1 == None:
        field1 = field
    if field2 == None:
        field2 = field

    first_rat = first.get_rat(field1)
    second_rat = second.get_rat(field2)

    if field1 == 'P1_LCCS' or field2 == 'P1_LCCS':
        normalize(first_rat, simplify)
    if field2 == 'P2_LCCS' or field2 == 'P1_LCCS':
        normalize(second_rat, simplify)

    classes = sorted(list(set(list(first_rat) + list(second_rat))))
    matrix = {}
    colors = {}
    random.seed()
    for i in range(0,len(classes)):
        for j in range(0,len(classes)):
            matrix[(i,j)] = j + i * len(classes)
            if i != j:
                colors[(i,j)] = [random.randint(1,255),random.randint(1,255), \
                        random.randint(1,255)]
            else:
                colors[(i,j)] = [0,0,0] 

    if verbose:
        print 'Classes(%d): ' % len(classes)
        print classes 
        print 'Transition Matrix(%d): ' % len(matrix)
        print matrix
        print 'Colors(%d): ' % len(colors)
        print colors

    file_name, file_ext = os.path.splitext(out_file)
    wb = xls.Workbook(file_name + '.xlsx')
    ws = wb.add_worksheet('Legend')

    st = wb.add_format()
    st.set_bold()
    for row in range(0,len(classes)):
        ws.write(xl_rowcol_to_cell(row+1,0),classes[row],st)
    for col in range(0,len(classes)):
        ws.write(xl_rowcol_to_cell(0,col+1),classes[col],st)

    for i in range(0,len(classes)):
        for j in range(0,len(classes)):
            st = wb.add_format()
            st.set_pattern(1)
            if i != j:
                st.set_bg_color('#%02X%02X%02X' % (colors[(i,j)][0], colors[(i,j)][1], \
                            colors[(i,j)][2]))
                st.set_font_color('#%02X%02X%02X' % (contrast_color(colors[(i,j)][0]), \
                        contrast_color(colors[(i,j)][1]), \
                        contrast_color(colors[(i,j)][2])))
            else:
                st.set_bg_color('black')
                st.set_font_color('white')
            ws.write(xl_rowcol_to_cell(1+i,1+j),matrix[(i,j)],st)
    if verbose:
        print 'Writing %s.xlsx worksheet' % file_name
    wb.close()

    if ulx is None:
        ulx = fi.ulx
        uly = fi.uly
        lrx = fi.lrx
        lry = fi.lry
            
    if psize_x is None:
        psize_x = fi.geotransform[1]
        psize_y = fi.geotransform[5]

    if band_type is None:
            band_type = fi.band_type
            if bTargetAlignedPixels:
                ulx = math.floor(ulx / psize_x) * psize_x
                lrx = math.ceil(lrx / psize_x) * psize_x
                lry = math.floor(lry / -psize_y) * -psize_y
                uly = math.ceil(uly / -psize_y) * -psize_y
    
    geotransform = [ulx, psize_x, 0, uly, 0, psize_y]

    xsize = int((lrx - ulx) / geotransform[1] + 0.5)
    ysize = int((lry - uly) / geotransform[5] + 0.5)
    if verbose:
        print 'Creating file ' + out_file
    t_fh = Driver.Create( out_file, xsize, ysize, 1, band_type )

    t_fh.SetGeoTransform( geotransform )
    t_fh.SetProjection( fi.projection )

    if not no_pct:
        color = gdal.ColorTable()
        for i in range(0,len(classes)):
            for j in range(0,len(classes)):
                color.SetColorEntry(matrix[(i,j)],(colors[(i,j)][0], \
                    colors[(i,j)][1], colors[(i,j)][2], 255))
        t_fh.GetRasterBand(1).SetRasterColorTable(color)

    ft = file_transitions(first,second)
    ft.transitions( t_fh, first_rat, second_rat, matrix, classes)
    t_fh = None

if __name__ == '__main__':
    sys.exit(main())
