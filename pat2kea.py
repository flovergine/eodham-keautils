#!/usr/bin/env python
#
# Copyright (c) 2013 Francesco P Lovergine <frankie(at)gisgeek.org>.
#
# Licensed under the EUPL 1.1
# See 
#   https://joinup.ec.europa.eu/software/page/eupl/licence-eupl
# for the text of license.

# This software is distributed WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the above copyright notices for more information.
#

try:
    from osgeo import gdal
except ImportError:
    import gdal

from gdalconst import *

try:
    progress = gdal.TermProgress_nocb
except:
    progress = gdal.TermProgress

try:
    import numpy as Numeric
except ImportError:
    import Numeric

import sys
import os.path
import re
import math
import random

try:
    import xlsxwriter as xls
except ImportError:
    print 'Missing XlsxWriter module'
    sys.exit(1)

from xlsxwriter.utility import xl_rowcol_to_cell

try:
    from rios import rat
except ImportError:
    print 'Missing Rios module'
    sys.exit(1)

__version__ = 1.0

verbose = False
simplify = False

def usage():
    print('''Usage: pat2kea.py [-o output_file] 
              [-ps pixelsize_x pixelsize_y] [-v] [-V] [-noct]
              [-ul_lr ulx uly lrx lry] [-f field] 
              [-ot datatype] image
''')    
    
def raster_copy( s_fh, s_xoff, s_yoff, s_xsize, s_ysize, s_band_n,
                 t_fh, t_xoff, t_yoff, t_xsize, t_ysize, t_band_n,
                 nodata=None ):

    if nodata is not None:
        return raster_copy_with_nodata(
            s_fh, s_xoff, s_yoff, s_xsize, s_ysize, s_band_n,
            t_fh, t_xoff, t_yoff, t_xsize, t_ysize, t_band_n,
            nodata )

    if verbose != 0:
        print('Copy %d,%d,%d,%d to %d,%d,%d,%d.' \
              % (s_xoff, s_yoff, s_xsize, s_ysize,
             t_xoff, t_yoff, t_xsize, t_ysize ))

    s_band = s_fh.GetRasterBand( s_band_n )
    t_band = t_fh.GetRasterBand( t_band_n )

    data = s_band.ReadRaster( s_xoff, s_yoff, s_xsize, s_ysize,
                             t_xsize, t_ysize, t_band.DataType )
    t_band.WriteRaster( t_xoff, t_yoff, t_xsize, t_ysize,
                        data, t_xsize, t_ysize, t_band.DataType )
        

    return 0
    
def raster_copy_with_nodata( s_fh, s_xoff, s_yoff, s_xsize, s_ysize, s_band_n,
                             t_fh, t_xoff, t_yoff, t_xsize, t_ysize, t_band_n,
                             nodata ):
    try:
        import numpy as Numeric
    except ImportError:
        import Numeric
    
    if verbose != 0:
        print('Copy %d,%d,%d,%d to %d,%d,%d,%d.' \
              % (s_xoff, s_yoff, s_xsize, s_ysize,
             t_xoff, t_yoff, t_xsize, t_ysize ))

    s_band = s_fh.GetRasterBand( s_band_n )
    t_band = t_fh.GetRasterBand( t_band_n )

    data_src = s_band.ReadAsArray( s_xoff, s_yoff, s_xsize, s_ysize,
                                   t_xsize, t_ysize )
    data_dst = t_band.ReadAsArray( t_xoff, t_yoff, t_xsize, t_ysize )

    nodata_test = Numeric.equal(data_src,nodata)
    to_write = Numeric.choose( nodata_test, (data_src, data_dst) )

    t_band.WriteArray( to_write, t_xoff, t_yoff )

    return 0
    
class file_info:

    def init_from_name(self, filename):
        fh = gdal.Open( filename,GA_ReadOnly )
        if fh is None:
            return 0

        self.filename = filename
        self.bands = fh.RasterCount
        self.xsize = fh.RasterXSize
        self.ysize = fh.RasterYSize
        self.band_type = fh.GetRasterBand(1).DataType
        self.projection = fh.GetProjection()
        self.geotransform = fh.GetGeoTransform()
        self.ulx = self.geotransform[0]
        self.uly = self.geotransform[3]
        self.lrx = self.ulx + self.geotransform[1] * self.xsize
        self.lry = self.uly + self.geotransform[5] * self.ysize

        ct = fh.GetRasterBand(1).GetRasterColorTable()
        if ct is not None:
            self.ct = ct.Clone()
        else:
            self.ct = None

        return 1

    def get_filename(self):
        return self.filename

    def report( self ):
        if verbose:
            print('Filename: '+ self.filename)
            print('File Size: %dx%dx%d' \
                  % (self.xsize, self.ysize, self.bands))
            print('Pixel Size: %f x %f' \
                  % (self.geotransform[1],self.geotransform[5]))
            print('UL:(%f,%f)   LR:(%f,%f)' \
                  % (self.ulx,self.uly,self.lrx,self.lry))
            print('Proj: %s' + self.projection)

    def copy_into( self, t_fh, s_band = 1, t_band = 1, nodata_arg=None ):
        t_geotransform = t_fh.GetGeoTransform()
        t_ulx = t_geotransform[0]
        t_uly = t_geotransform[3]
        t_lrx = t_geotransform[0] + t_fh.RasterXSize * t_geotransform[1]
        t_lry = t_geotransform[3] + t_fh.RasterYSize * t_geotransform[5]

        # figure out intersection region
        tgw_ulx = max(t_ulx,self.ulx)
        tgw_lrx = min(t_lrx,self.lrx)
        if t_geotransform[5] < 0:
            tgw_uly = min(t_uly,self.uly)
            tgw_lry = max(t_lry,self.lry)
        else:
            tgw_uly = max(t_uly,self.uly)
            tgw_lry = min(t_lry,self.lry)
        
        # do they even intersect?
        if tgw_ulx >= tgw_lrx:
            return 1
        if t_geotransform[5] < 0 and tgw_uly <= tgw_lry:
            return 1
        if t_geotransform[5] > 0 and tgw_uly >= tgw_lry:
            return 1
            
        # compute target window in pixel coordinates.
        tw_xoff = int((tgw_ulx - t_geotransform[0]) / t_geotransform[1] + 0.1)
        tw_yoff = int((tgw_uly - t_geotransform[3]) / t_geotransform[5] + 0.1)
        tw_xsize = int((tgw_lrx - t_geotransform[0])/t_geotransform[1] + 0.5) \
                   - tw_xoff
        tw_ysize = int((tgw_lry - t_geotransform[3])/t_geotransform[5] + 0.5) \
                   - tw_yoff

        if tw_xsize < 1 or tw_ysize < 1:
            return 1

        # Compute source window in pixel coordinates.
        sw_xoff = int((tgw_ulx - self.geotransform[0]) / self.geotransform[1])
        sw_yoff = int((tgw_uly - self.geotransform[3]) / self.geotransform[5])
        sw_xsize = int((tgw_lrx - self.geotransform[0]) \
                       / self.geotransform[1] + 0.5) - sw_xoff
        sw_ysize = int((tgw_lry - self.geotransform[3]) \
                       / self.geotransform[5] + 0.5) - sw_yoff

        if sw_xsize < 1 or sw_ysize < 1:
            return 1

        # Open the source file, and copy the selected region.
        s_fh = gdal.Open( self.filename )

        return \
            raster_copy( s_fh, sw_xoff, sw_yoff, sw_xsize, sw_ysize, s_band,
                         t_fh, tw_xoff, tw_yoff, tw_xsize, tw_ysize, t_band,
                         nodata_arg )

def names_to_fileinfos( names ):

    file_infos = []
    for name in names:
        fi = file_info()
        if fi.init_from_name( name ) == 1:
            file_infos.append( fi )

    return file_infos

def main( argv=None ):

    global verbose, __version__
    format = 'KEA'
    out_file_ext = '.kea'
    out_file = None
    bTargetAlignedPixels = False
    no_pct = False
    names = []
    ulx = None
    psize_x = None
    band_type = None
    field = 'P1_LCCS'

    gdal.AllRegister()
    if argv is None:
        argv = sys.argv
    argv = gdal.GeneralCmdLineProcessor( argv )
    if argv is None:
        sys.exit( 0 )

    i = 1
    while i < len(argv):
        arg = argv[i]

        if arg == '-o':
            i = i + 1
            out_file = argv[i]

        elif arg == '-v':
            verbose = True

        elif arg == '-ot':
            i = i + 1
            band_type = gdal.GetDataTypeByName( argv[i] )
            if band_type == gdal.GDT_Unknown:
                print('Unknown GDAL data type: %s' % argv[i])

        elif arg == '-ps':
            psize_x = float(argv[i+1])
            psize_y = -1 * abs(float(argv[i+2]))
            i = i + 2

        elif arg == '-ul_lr':
            ulx = float(argv[i+1])
            uly = float(argv[i+2])
            lrx = float(argv[i+3])
            lry = float(argv[i+4])
            i = i + 4

        elif arg == '-f':
            i = i + 1
            field = argv[i]

        elif arg == '-V':
            print '%s' % __version__
            sys.exit(1)

        elif arg == '-s':
            simplify = True

        elif arg[:1] == '-':
            print('Unrecognized command option: %s' % arg)
            usage()
            sys.exit(1)

        else:
            names.append(arg)

        i = i + 1

    if len(names) == 0:
        print('No input file selected')
        usage()
        sys.exit(1)

    Driver = gdal.GetDriverByName(format)
    if Driver is None:
        print('Format driver %s not found, pick a supported driver.' % format)
        sys.exit( 1 )
    metadata = Driver.GetMetadata()

    file_infos = names_to_fileinfos( names )

    for fi in file_infos:
        fi.report()

    #
    # This is not a true general LUT, it could require adaptations for
    # specific classifications :-/
    #
    # Alta Murgia
    #
    #classes = { 0:'NA', 1:'A11.A3', 2:'A11.A3.A4', 3:'A12.A2.A6.E6', 4:'A12.A1.D1.E1', 
    #            5:'A12.A1.D1.E2', 6:'A12.A1.D2.E1', 7:'A12.A1.D2.E2',
    #            8:'A12.A2', 9:'B15', 10:'B15.A2.A6', 
    #            11:'B16', 12:'SH', 13:'A11.A1|A2.A7.A9', 14:'A11.A1|A2.A7.A10', 15:'A11.A3.A5|A11.A2.A7.A10.W8', 
    #            16:'B28.B27' }
    # 
    # Cesine
    #
    classes = { 0:'NA', 
                1:'B15', 
                2:'A11', 
                3:'A12',
                4:'A24', 
                5:'B16', 
                6:'B28'  }
    if verbose:
        print 'Classes(%d): ' % len(classes)
        print classes 

    for fi in file_infos:
        if out_file == None:
           out_file, file_ext = os.path.splitext(fi.filename)
           out_file = out_file + out_file_ext

        if ulx is None:
            ulx = fi.ulx
            uly = fi.uly
            lrx = fi.lrx
            lry = fi.lry
                
        if psize_x is None:
            psize_x = fi.geotransform[1]
            psize_y = fi.geotransform[5]

        if band_type is None:
                band_type = fi.band_type
                if bTargetAlignedPixels:
                    ulx = math.floor(ulx / psize_x) * psize_x
                    lrx = math.ceil(lrx / psize_x) * psize_x
                    lry = math.floor(lry / -psize_y) * -psize_y
                    uly = math.ceil(uly / -psize_y) * -psize_y
        
        geotransform = [ulx, psize_x, 0, uly, 0, psize_y]

        xsize = int((lrx - ulx) / geotransform[1] + 0.5)
        ysize = int((lry - uly) / geotransform[5] + 0.5)
        if verbose:
            print 'Creating file ' + out_file
            print 'xsize %d, ysize %d' % (xsize,ysize)
            print 'type %s' % band_type
        t_fh = Driver.Create(out_file, xsize, ysize, 1, band_type)

        t_fh.SetGeoTransform(geotransform)
        t_fh.SetProjection(fi.projection)

        fi.copy_into(t_fh,1,1)

        band = t_fh.GetRasterBand(1)
        band.SetRasterColorTable(fi.ct)
        myrat = gdal.RasterAttributeTable()
        myrat.CreateColumn(field,gdal.GFT_String,gdal.GFU_Generic)
        myrat.SetRowCount(len(classes))
        for i in range(len(classes)):
            myrat.SetValueAsString(i,0,classes[i])
        band.SetDefaultRAT(myrat)
        t_fh = None

if __name__ == '__main__':
    sys.exit(main())

